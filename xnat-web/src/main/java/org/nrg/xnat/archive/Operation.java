package org.nrg.xnat.archive;

public enum Operation {
    Archive,
    Delete,
    Move,
    Rebuild,
    Separate
}
