/*
 * web: org.nrg.xnat.helpers.transactions.HTTPSessionStatusManagerQueue
 * XNAT http://www.xnat.org
 * Copyright (c) 2005-2017, Washington University School of Medicine and Howard Hughes Medical Institute
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnat.helpers.transactions;

import org.nrg.xnat.status.StatusList;

import javax.servlet.http.HttpSession;

public class HTTPSessionStatusManagerQueue implements PersistentStatusQueueManagerI {
    public HTTPSessionStatusManagerQueue(final HttpSession session) {
        _session = session;
    }

    @Override
    public StatusList storeStatusQueue(final String id, final StatusList statusList) throws IllegalArgumentException {
        _session.setAttribute(TransactionUtils.buildTransactionID(id), statusList);
        return statusList;
    }

    @Override
    public StatusList retrieveStatusQueue(final String id) throws IllegalArgumentException {
        return (StatusList) _session.getAttribute(TransactionUtils.buildTransactionID(id));
    }

    @Override
    public StatusList deleteStatusQueue(final String id) throws IllegalArgumentException {
        final StatusList statusList = retrieveStatusQueue(TransactionUtils.buildTransactionID(id));
        _session.removeAttribute(id);
        return statusList;
    }

    private final HttpSession _session;
}
